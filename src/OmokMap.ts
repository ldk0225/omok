import Stone from "./stone";

const mapsize = 20;

class OmokMap {
    private map: Array<Stone>;

    constructor() {
        this.map = new Array(Math.pow(mapsize, 2)).fill(null);
    }

    set setMap(stone:Stone) {
        this.map[stone.getPos['x']][stone.getPos['y']] = stone;
    }

    get getMap() {
        return this.map;
    }
}

export default OmokMap;