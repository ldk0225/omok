import Stone from "./stone";
import OmokMap from "./OmokMap";

class Manager {
    private map:OmokMap;
    private turn:boolean=false;

    constructor() {
        this.map = new OmokMap;
    }

    get render():Array<Stone> {
       return this.map.getMap;
    }
}

export default new Manager;