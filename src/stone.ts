class Stone {
    static GET(type, pos) { return new Stone(type,pos);}
    private turn:boolean;
    private pos:number;

    constructor(_type, _pos) {
        this.turn = _type;
        this.pos = _pos;
    }

    setPos(pos:number) {
        this.pos=pos;
    }

    get getPos():number {
        return this.pos;
    }

    get getTurn():boolean {
        return this.turn;
    }
}

export default Stone;