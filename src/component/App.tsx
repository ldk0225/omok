import React, {useEffect, useState} from "react";

import classNames from 'classnames/bind';
import style from './App.scss';

import Stone from "../stone";

const dx= [0, 0, -1, 1, 1, 1, -1, -1];
const dy = [1,-1, 0, 0, 1, -1, 1, -1]

const cx = classNames.bind(style);

const mapsize = Math.pow(10, 2);
const ispossible = (curr, curc, nx, ny, map):boolean => (nx>=0 && ny>=0 && nx<mapsize && ny<mapsize) && map[nx*mapsize+ny] instanceof Stone && (map[nx*mapsize + ny].getTurn == map[curr*mapsize+curc].getTurn);


const App = ():JSX.Element => {
    const [omokMap, setOmokMap] = useState<Array<Stone>>(new Array(mapsize).fill(null));
    const [turn, setTurn] = useState<boolean>(false);
    const [end, setEnd]= useState<boolean>(false);
    const checkEnd = (pos?:number, dir?:number, depth?:number, turn?:boolean) => {
                if(depth===4) {
                    setEnd(true);
                    return;
                }
                (ispossible(Math.floor(pos/mapsize), pos%mapsize, Math.floor(pos/mapsize)+dx[dir], pos%mapsize + dy[dir], omokMap)) &&
                checkEnd((Math.floor(pos/mapsize)+dx[dir])*mapsize+pos%mapsize+dy[dir], dir, depth+1, turn);
    };

    const onClickOmokMap = (pos:number, turn:boolean):void => {
        if(omokMap[pos]) return;
        omokMap[pos] = new Stone(turn, pos);
        for(let i=0; i<8; i++)
            (ispossible(Math.floor(pos/mapsize), pos%mapsize, (Math.floor(pos/mapsize)+dx[i]), (pos%mapsize+dy[i]), omokMap)) &&
            checkEnd(Math.floor(pos/mapsize)*mapsize+pos%mapsize, i, 0, turn);

        setTurn(!turn);
        setOmokMap(omokMap);
    };

    end && alert('종료');
    return <div className={cx('omok')}>
            {omokMap.map((stone:Stone, idx:number) => <div key={idx} className={cx('map')} onClick={e => onClickOmokMap(idx, turn)}>
                <div className={cx((stone instanceof Stone) && (stone.getTurn ? 'my': 'you'))}></div>
            </div>)}
        </div>;
};

export default App;